FROM balenalib/raspberrypi3-openjdk:11-jdk-build as builder

MAINTAINER Amael H. <amael@heiligtag.com>

ENV SONAR_VERSION=8.3.1.34397

RUN apt-get clean \
    && apt-get update \
    && apt-get install -y \
    wget \
    unzip \
    && cd / \
    && wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-${SONAR_VERSION}.zip \
    && unzip sonarqube-${SONAR_VERSION}.zip \
    && mv /sonarqube-${SONAR_VERSION} /sonarqube

#################################

FROM balenalib/raspberrypi3-openjdk:11-run

WORKDIR /

RUN apt-get update \
    && apt-get install -y \
    libjna-java \
    && rm -rf /var/lib/apt/lists/*

# Copy Sonarqube from builder
COPY --from=builder /sonarqube/ /sonarqube/
COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

# Configure system
RUN echo "vm.max_map_count=262144" >> /etc/sysctl.conf

# Configure elastic
COPY elasticsearch /sonarqube/elasticsearch/bin/elasticsearch
COPY config/elasticsearch.yml /sonarqube/elasticsearch/config/elasticsearch.yml

# Add file
COPY healthcheck.sh /

HEALTHCHECK CMD /healthcheck.sh --retries=1 --start-period=5m

# Volumes, ports and entrypoints
VOLUME /sonarqube/extensions /sonarqube/logs/
EXPOSE 9000

ENTRYPOINT ["/entrypoint.sh"]

CMD ["app:start"]
