#!/bin/bash

#
# Sonarqube image health check
#

if [[ `ps -ef | grep sonar-application | grep -v grep | wc -l` -ne 1 ]]
then
	echo "Missing Sonar process"
	exit 1
fi

if [[ `ps -ef | grep org.sonar.ce.app.CeServer | grep -v grep | wc -l` -ne 1 ]]
then
	echo "Missing CeServer process"
	exit 1
fi

if [[ `ps -ef | grep org.sonar.server.app.WebServer | grep -v grep | wc -l` -ne 1 ]]
then
	echo "Missing WebServer process"
	exit 1
fi

if [[ `ps -ef | grep org.elasticsearch.bootstrap.Elasticsearch | grep -v grep | wc -l` -ne 1 ]]
then
	echo "Missing Elasticsearch process"
	exit 1
fi

# Else everything is healthy
exit 0
