# Sonarqube

This Docker container implements a Sonarqube Server for Raspberry pi 3+, based on balenalib base image

 
### Installation from [Docker registry hub](https://registry.hub.docker.com/u/amaelh/rpi-sonarqube/).

You can download the image with the following command:

```bash
docker pull amaelh/rpi-sonarqube
```

# How to use this image

## Run this container

If you'd like to run the container, you must download the plugins to a folder and pass the folder path to the container as below :

docker-compose.yml : 
```
version: "2.0"

services:

  postgresql:
    image: postgres:11
    restart: always
    environment:
      - POSTGRES_USER=sonar
      - POSTGRES_PASSWORD=sonar_password
      - POSTGRES_DB=sonar
    volumes:
      - ./conf/postgresql.conf:/etc/postgresql/postgresql.conf:ro
      - ~/data/sonarqube/postgresql:/var/lib/postgresql/data:rw
      - /etc/localtime:/etc/localtime:ro
    command: -c 'config_file=/etc/postgresql/postgresql.conf'
    networks:
      sonar-network:
        aliases:
         - db

  sonarqube:
    image: amaelh/rpi-sonarqube:latest
    restart: always
    ports:
      - "9080:9000"
    environment:
      - DB_USER=sonar
      - DB_PASS=sonar_password
      - DB_NAME=sonar
      - DB_TYPE=POSTGRES
    volumes:
      - ~/data/sonarqube/plugins:/sonarqube/extensions/plugins:rw
      - ~/data/sonarqube/logs:/sonarqube/logs:rw
      - /etc/localtime:/etc/localtime:ro
    networks:
      sonar-network:
        aliases:
         - qube

networks:
  sonar-network:

```

### Minimal configuration files to make this image work :
----

postgresql.conf : 
```
listen_addresses = '*'
```

----

/etc/sysctl.d/elastic-container-memory.conf
```
vm.max_map_count=262144
```


## Exposed ports and volumes
----

The image exposes port `9000`. Also, exports two volumes: `/sonarqube/extensions` and `/sonarqube/logs/`, used to store all the plugins and the sonarqube logs.


## Environment variables
----

This image uses environment variables to allow the configuration of some parameteres at run time:

* Variable name: `DB_USER`
* Default value: sonar
* Accepted values: a valid user created in the database.
* Description: database user.
----

* Variable name: `DB_PASS`
* Default value: sonar_password
* Accepted values: a valid password for the user created in the database.
* Description: database password.
----

* Variable name: `DB_NAME`
* Default value: sonar
* Accepted values: database name where the container must connect.
* Description: database name.
----

* Variable name: `DB_TYPE`
* Default value: MYSQL
* Accepted values: database type. MYSQL, POSTGRES, MSSQL
* Description: database types: mysql, postgres or microsoft sql server
----

* Variable name: `LANGUAGE_VERSION`
* Default value: 1.1
* Description: version of portuguese language plugin.
----

# Plugins:
* [GitHub Authentication Plugin](https://docs.sonarqube.org/display/PLUG/GitHub+Authentication+Plugin)
* [Git Plugin](https://docs.sonarqube.org/display/PLUG/Git+Plugin)
* [Sonar Java](https://docs.sonarqube.org/display/PLUG/SonarJava)
* [Sonar C Family for Cpp Plugin](https://www.sonarsource.com/products/codeanalyzers/sonarcfamilyforcpp.html)
* [Sonar Php Plugin](https://www.sonarsource.com/products/codeanalyzers/sonarphp.html)
* [Sonar C Sharp Plugin](https://www.sonarsource.com/products/codeanalyzers/sonarcsharp.html)
* [Sonar Pl Sql Plugin](https://www.sonarsource.com/products/codeanalyzers/sonarplsql.html)
* [Sonar TSql Plugin](https://www.sonarsource.com/products/codeanalyzers/sonartsql.html)
* [Sonar VB6 Plugin](https://www.sonarsource.com/products/codeanalyzers/sonarvb6.html)
* [Sonar VB Net Plugin](https://www.sonarsource.com/products/codeanalyzers/sonarvbnet.html)
* [Sonar Web Plugin](https://www.sonarsource.com/products/codeanalyzers/sonarweb.html)
* [Sonar Xml Plugin](https://www.sonarsource.com/products/codeanalyzers/sonarxml.html)
* [Sonar TS Plugin](https://www.sonarsource.com/products/codeanalyzers/sonarts.html)
* [Sonar CSS Plugin](https://github.com/kalidasya/sonar-css-plugin)
* [Sonar Android Plugin](https://github.com/ofields/sonar-android)
